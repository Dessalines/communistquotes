 #!/usr/bin/python3

import os
from mastodon import Mastodon
import csv
import itertools
import random
import os

instance = os.environ['MASTODON_INSTANCE']
username = os.environ['MASTODON_USERNAME']

mastodon_api = None
# Create application if it does not exist
# TODO: store this file in volume
if not os.path.isfile(instance+'.secret'):
    if Mastodon.create_app(
        'tootbot',
        api_base_url='https://'+instance,
        to_file = instance+'.secret'
    ):
        print('tootbot app created on instance '+instance)
    else:
        print('failed to create app on instance '+instance)
        exit(1)

    mastodon_api = Mastodon(
        client_id=instance+'.secret',
        api_base_url='https://'+instance
    )
    mastodon_api.log_in(
        username=username,
        password=os.environ['MASTODON_PASSWORD'],
        scopes=['read', 'write'],
        to_file=username+".secret"
    )

with open('marxistquotes.csv') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='`', skipinitialspace=True)
    row = random.choice(list(csvreader))

    text = '<p>{}</p>&nbsp;&nbsp;&nbsp;&nbsp;- {}<a href={}>{}</a>'.format(row[0], row[1], row[2], row[3])
    toot = mastodon_api.status_post(text, visibility='public', content_type='text/html')
