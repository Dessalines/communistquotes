# Communist Quotes Bot

A Fediverse bot that posts random quotes from different communists.

You can check it out here: https://impenetrable.fortress.promo/communistquotes

## Contribute Quotes

Edit the file [communistquotes/marxistquotes.csv](communistquotes/marxistquotes.csv) and
make an MR.

## Compile and Run

First, copy `env.example` to `env`, then enter your actual login information. To run the bot,
execute the following commands:

```bash
cd communistquotes
sudo docker run -it --rm --name communistquotes --env-file env communistquotes
sudo docker run -it --rm --name communistquotes --env-file ../env communistquotes
```

## Deploy

Copy `inventory.example` to `inventory`, and enter your server's ssh address. Then run the following command:
```
ansible-playbook ansible.yml --become
```